package net.sssubtlety.custom_piglin_bartering;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.sssubtlety.custom_piglin_bartering.mixin_helpers.PiglinBrainMixinAccessor;

public class CustomPiglinBarteringInit implements ModInitializer {
    @Override
    public void onInitialize() {
        ServerLifecycleEvents.SERVER_STARTED.register(PiglinBrainMixinAccessor::buildBarters);
        ServerLifecycleEvents.END_DATA_PACK_RELOAD.register((server, world, bool) -> PiglinBrainMixinAccessor.buildBarters(server));
    }
}
