Minecraft mod for the [Fabric mod loader](fabricmc.net) and Minecraft 1.16. 

Allows datapacks to:
1) ~~define a list of items that piglins are attracted to~~ removed because this is now handled by the base game's `piglin_loved` item tag
2) define a list of items that piglins will barter for
3) map each barterable item to a loot table that represents possible bartering returns

Requires the [fabric-api](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files/all?filter-game-version=1738749986%3a70886). 
Latest version is for both 1.16 and 1.16.1